import UIKit
import Flutter
import FirebaseAuth
import Firebase


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var verificationID = ""
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        
        FirebaseApp.configure()
        let controller = window?.rootViewController as! FlutterViewController
        let channel = FlutterMethodChannel(name: "channel", binaryMessenger: controller.binaryMessenger)
        
        channel.setMethodCallHandler({ (call:FlutterMethodCall, result : @escaping FlutterResult) -> Void in
            
            if(call.method == "verify_phone"){
                
                let args = call.arguments as? Dictionary<String, Any>
                self.verifyPhone(args?["phone"] as? String ?? "" ){ response in
                    result(response)
                }
            }else if(call.method == "verify_code"){
                let args = call.arguments as? Dictionary<String, Any>
                self.verifyCode(args?["code"] as? String ?? "" ){ response in
                    result(response)
                }
            }
            
        })
        GeneratedPluginRegistrant.register(with: self)
        // Use Firebase library to configure APIs
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func verifyPhone(_ phone:String,completion: @escaping (String) -> Void){
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                completion(error.localizedDescription)
                return
            }
            if let id = verificationID {
                self.verificationID = id
                completion("")
            }
        }
    }
    
    func verifyCode(_ code:String,completion: @escaping (String) -> Void){
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: code)
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                let authError = error as NSError
                print(authError.description)
                completion("ERROR " + authError.description)
                return
            }
            
            // User has signed in successfully and currentUser object is valid
            let currentUserInstance = Auth.auth().currentUser
            completion(currentUserInstance?.refreshToken ?? "")
        }
    }
    
    
}
