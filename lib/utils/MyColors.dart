import 'package:cu_jobs/utils/extension.dart';
import 'package:flutter/material.dart';

class MyColors {

   static final Color c_a43 = HexColor.fromHex("#1d1a43");
   static final Color c_78c = HexColor.fromHex("#79778c");
   static final Color c_e53 = HexColor.fromHex("#312e53");
   static final Color c_9df = HexColor.fromHex("#d9d9df");
   static final Color c_fec = HexColor.fromHex("#796fec");
   static final Color c_0f2 = HexColor.fromHex("#a5a0f2");
   static final Color c_fee = HexColor.fromHex("#897fee");
   static final Color c_498 = HexColor.fromHex("#858498");
   static final Color c_5e3 = HexColor.fromHex("#7165E3");
   static final Color c_7f7 = HexColor.fromHex("#f7f7f7");
   static final Color c_948 = HexColor.fromHex("#2C2948");
   static final Color c_b26 = HexColor.fromHex("#191B26");
   static final Color c_b60 = HexColor.fromHex("#595B60");
   static final Color c_5fa = HexColor.fromHex("#f9f9fb");
   static final Color c_f93 = HexColor.fromHex("#8E8F93");
   static final Color c_763 = HexColor.fromHex("#0BC763");
   static final Color c_fdf = HexColor.fromHex("#DFDFDF");
   static final Color c_2c2 = HexColor.fromHex("#C2C2C2");


}