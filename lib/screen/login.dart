import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String phone = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppLocalizations.of(context).translate("sign_in"),
              style: TextStyle(color: MyColors.c_a43, fontSize: 35,fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(00, 50, 0, 0),
              child: Text(
                AppLocalizations.of(context).translate("enter_valid_phone"),
                style: TextStyle(color: MyColors.c_78c, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Card(
              color: Colors.white,
              child: new Container(
                padding: const EdgeInsets.all(8),
                child: new Row(
                  children: <Widget>[
                    Image.asset(
                      "images/egypt.png",
                      width: 30,
                      height: 20,
                    ),
                    Text(
                      "+20",
                      style: TextStyle(color: MyColors.c_e53, fontSize: 15,fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Container(
                        width: 1,
                        height: 30,
                        color: MyColors.c_9df,
                      ),
                    ),
                    Expanded(
                      child: new TextField(
                        cursorColor: Colors.black,
                        keyboardType: TextInputType.phone,
                        onChanged: (phone){
                          this.phone = phone;
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintText: "+00 000 0000",
                            hintStyle:
                                TextStyle(color: MyColors.c_e53, fontSize: 15,fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  Navigator.of(context).pushNamed("/verify_account");
                },
                child: Text(
                  AppLocalizations.of(context).translate("sign_in"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            //CircularProgressIndicator()
          ],
        ),
      ),
    );
  }
}
