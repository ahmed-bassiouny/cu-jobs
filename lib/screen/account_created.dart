import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class AccountCreatedScreen extends StatefulWidget {
  @override
  _AccountCreatedScreenState createState() => _AccountCreatedScreenState();
}

class _AccountCreatedScreenState extends State<AccountCreatedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            Image.asset("images/account_created.png",width: 200,height: 200,),
            SizedBox(height: 100,),
            Text(
              AppLocalizations.of(context).translate("account_created"),
              style: TextStyle(color: MyColors.c_a43, fontSize: 35),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 30,),
            Text(
                AppLocalizations.of(context).translate("account_created_info"),
                style: TextStyle(color: MyColors.c_78c, fontSize: 15),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 120,),
              ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  Navigator.of(context).pushNamed("/blocked");
                },
                child: Text(
                  AppLocalizations.of(context).translate("continue"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            )
          ],),
        ),
      ),
    );
  }
}