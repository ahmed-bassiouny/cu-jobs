import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class CompleteProfileScreen extends StatefulWidget {
  @override
  _CompleteProfileScreenState createState() => _CompleteProfileScreenState();
}

class _CompleteProfileScreenState extends State<CompleteProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 100,
              ),
              Text(
                AppLocalizations.of(context).translate("welcome"),
                style: TextStyle(
                    color: MyColors.c_a43,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 50),
                child: Text(
                  AppLocalizations.of(context).translate("please_provide"),
                  style: TextStyle(color: MyColors.c_78c, fontSize: 16),
                  textAlign: TextAlign.center,
                ),
              ),
              TextField(
                maxLines: 1,
                keyboardType: TextInputType.name,
                cursorColor: MyColors.c_fec,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate("full_name"),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: MyColors.c_fec),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                maxLines: 1,
                keyboardType: TextInputType.emailAddress,
                cursorColor: MyColors.c_fec,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate("email"),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: MyColors.c_fec),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                maxLines: 1,
                keyboardType: TextInputType.text,
                cursorColor: MyColors.c_fec,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context).translate("password"),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: MyColors.c_fec),
                  ),
                ),
              ),
              Theme(
                data: Theme.of(context).copyWith(
                  disabledColor: MyColors.c_a43,
                ),
                child: Row(
                  children: [
                    Text(
                      AppLocalizations.of(context).translate("gender"),
                      style: TextStyle(fontSize: 16, color: MyColors.c_a43),
                    ),
                    Radio(
                      value: true,
                      groupValue: true,
                      onChanged: null,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("male"),
                      style: TextStyle(fontSize: 16, color: MyColors.c_a43),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Radio(
                        value: true,
                        groupValue: false,
                        onChanged: null,
                        activeColor: MyColors.c_fec),
                    Text(
                      AppLocalizations.of(context).translate("female"),
                      style: TextStyle(fontSize: 16, color: MyColors.c_a43),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 80,
              ),
              ButtonTheme(
                minWidth: 300.0,
                height: 50,
                child: RaisedButton(
                  color: MyColors.c_fec,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onPressed: () {
                    Navigator.of(context).pushNamed("/account_created");
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("process"),
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
