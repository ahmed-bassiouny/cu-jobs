import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class VerifyAccountScreen extends StatefulWidget {
  VerifyAccountScreen({Key key}) : super(key: key);

  @override
  _VerifyAccountScreenState createState() => _VerifyAccountScreenState();
}

class _VerifyAccountScreenState extends State<VerifyAccountScreen> {
  String phone = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 100,
            ),
            Text(
              AppLocalizations.of(context).translate("verify_account"),
              style: TextStyle(
                  color: MyColors.c_a43,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 40, 0, 10),
              child: Text(
                AppLocalizations.of(context).translate("enter_4_digit"),
                style: TextStyle(color: MyColors.c_78c, fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            Text(
              "+000 00000",
              style: TextStyle(color: MyColors.c_0f2, fontSize: 16),
              textAlign: TextAlign.center,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 30),
                child: VerificationCode(
                  textStyle: TextStyle(fontSize: 20.0, color: Colors.black),
                  underlineColor: MyColors.c_fec,
                  keyboardType: TextInputType.number,
                  itemSize: 80,
                  length: 4,
                  // clearAll is NOT required, you can delete it
                  // takes any widget, so you can implement your design
                  clearAll: Text(
                    'clear all',
                    style: TextStyle(
                        fontSize: 14.0,
                        decoration: TextDecoration.underline,
                        color: Colors.blue[700]),
                  ),
                  onCompleted: (String value) {},
                  onEditing: (bool value) {
                    print("onEditing ${value}");
                  },
                ),
              ),
            ),
            Text(
              AppLocalizations.of(context).translate("didnt_received"),
              style: TextStyle(color: MyColors.c_498, fontSize: 16),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 80),
              child: Text(
                AppLocalizations.of(context).translate("resendـcode"),
                style: TextStyle(color: MyColors.c_fee, fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  Navigator.of(context).pushNamed("/complete_profile");
                },
                child: Text(
                  AppLocalizations.of(context).translate("process"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            //Center(child: CircularProgressIndicator()),
            SizedBox(
              height: 20,
            ),
            RichText(
              text: TextSpan(
                style: TextStyle(color: Colors.black, fontSize: 36),
                children: <TextSpan>[
                  TextSpan(
                      text:
                          AppLocalizations.of(context).translate("by_click"),
                      style: TextStyle(color: Colors.black, fontSize: 12)),
                  TextSpan(
                      text: AppLocalizations.of(context)
                          .translate("privacy_policy"),
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: MyColors.c_fec,
                          fontSize: 12),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          // single tapped
                        }),
                  TextSpan(
                      text: AppLocalizations.of(context).translate("and"),
                      style: TextStyle(color: Colors.black, fontSize: 12)),
                  TextSpan(
                      text: AppLocalizations.of(context).translate("terms"),
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: MyColors.c_fec,
                          fontSize: 12),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          // single tapped
                        }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
