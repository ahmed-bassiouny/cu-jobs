import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class EasyApplyScreen extends StatefulWidget {
  EasyApplyScreen({Key key}) : super(key: key);

  @override
  _EasyApplyScreenState createState() => _EasyApplyScreenState();
}

class _EasyApplyScreenState extends State<EasyApplyScreen> {
  String phone = "";

    void _modalBottomSheetMenu(){
        showModalBottomSheet(
            context: context,
            builder: (builder){
              return new ListView.separated(
                padding: const EdgeInsets.all(20),
                itemCount: 5,
                separatorBuilder: (context,index){
                  return Divider(color: Colors.grey,);
                },
                itemBuilder: (context,index){
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Engineering",style: TextStyle(
                      color: MyColors.c_948,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
              ),
                );
              });
            }
        );
      }

  Widget input(String txt,Function press) => InkWell(
        onTap: () {
          _modalBottomSheetMenu();
          press();
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: new BoxDecoration(
            color: MyColors.c_7f7,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(10),
            border: new Border.all(
              color: MyColors.c_7f7,
              width: 2.0,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).translate(txt),
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: MyColors.c_948,
                    fontSize: 16,
                    fontWeight: FontWeight.w800),
              ),
              Icon(
                Icons.arrow_drop_down,
                color: Colors.grey,
              )
            ],
          ),
        ),
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              AppLocalizations.of(context).translate("skip").toUpperCase(),
              style: TextStyle(
                  color: MyColors.c_5e3,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              AppLocalizations.of(context).translate("easy_apply"),
              style: TextStyle(
                  color: MyColors.c_a43,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 80,
            ),
            input("job_title",(){}),
            SizedBox(
              height: 20,
            ),
            input("interested",(){}),
            SizedBox(
              height: 20,
            ),
            input("level",(){}),
            SizedBox(
              height: 20,
            ),
            input("education",(){}),
            SizedBox(
              height: 20,
            ),
            input("resident_in",(){}),
            SizedBox(
              height: 60,
            ),
            ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  Navigator.of(context).pushNamed("/complete_profile");
                },
                child: Text(
                  AppLocalizations.of(context).translate("save"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            //Center(child: CircularProgressIndicator()),
          ],
        ),
      ),
    );
  }
}
