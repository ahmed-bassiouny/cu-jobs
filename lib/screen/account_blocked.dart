import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class AccountBlockedScreen extends StatefulWidget {
  @override
  _AccountBlockedScreenState createState() => _AccountBlockedScreenState();
}

class _AccountBlockedScreenState extends State<AccountBlockedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            Image.asset("images/block.png",width: 200,height: 200,),
            SizedBox(height: 100,),
            Text(
              AppLocalizations.of(context).translate("account_blocked"),
              style: TextStyle(color: MyColors.c_a43, fontSize: 35),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 30,),
            Text(
                AppLocalizations.of(context).translate("account_blocked_info"),
                style: TextStyle(color: MyColors.c_78c, fontSize: 15),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 120,),
              ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {},
                child: Text(
                  AppLocalizations.of(context).translate("close"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            )
          ],),
        ),
      ),
    );
  }
}