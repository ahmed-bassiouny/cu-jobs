import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new ListView.separated(
              padding: const EdgeInsets.all(20),
              itemCount: 5,
              separatorBuilder: (context, index) {
                return Divider(
                  color: Colors.grey,
                );
              },
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Engineering",
                    style: TextStyle(
                        color: MyColors.c_948,
                        fontSize: 20,
                        fontWeight: FontWeight.w600),
                  ),
                );
              });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.c_5fa,
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(8, 100, 8, 20),
        child: Column(
          children: [
            Text(
              "Hi, Ali",
              style: TextStyle(color: MyColors.c_b26, fontSize: 36),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              AppLocalizations.of(context).translate("what_kind"),
              style: TextStyle(color: MyColors.c_b60, fontSize: 21),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 12,
            ),
            Card(
              color: Colors.white,
              elevation: 8,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: new Container(
                padding: const EdgeInsets.fromLTRB(16, 8, 8, 8),
                child: InkWell(
                  onTap: () {
                    _modalBottomSheetMenu();
                  },
                  child: new Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Accounting",
                          style: TextStyle(
                              color: MyColors.c_b26,
                              fontSize: 21,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Card(
                        color: MyColors.c_fec,
                        elevation: 8,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Card(
              color: Colors.white,
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Image.asset("images/approve.png"),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 18,
                      ),
                      Text(
                        "PENDING",
                        style: TextStyle(color: MyColors.c_fec, fontSize: 13),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Awaiting admin approval",
                        style: TextStyle(color: MyColors.c_b26, fontSize: 21),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Your request will be reviewed within 24 hours",
                        style: TextStyle(color: MyColors.c_f93, fontSize: 18),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                    ],
                  ))
                ],
              ),
            ),
            SizedBox(height: 12),
            Card(
              color: Colors.white,
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(22.0, 0, 0, 0),
                    child: Text(
                      "Complete your profile",
                      style: TextStyle(
                          color: MyColors.c_5e3,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.check,
                      color: MyColors.c_763,
                    ),
                    title: Text(
                      "Basic Info",
                      style: TextStyle(color: MyColors.c_b26, fontSize: 21),
                    ),
                    subtitle: Text(
                      "Name, email, phone, gender",
                      style: TextStyle(color: MyColors.c_f93, fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.check,
                      color: MyColors.c_763,
                    ),
                    title: Text(
                      "Easy Apply Option",
                      style: TextStyle(color: MyColors.c_b26, fontSize: 21),
                    ),
                    subtitle: Text(
                      "Job title, intrested, level,…",
                      style: TextStyle(color: MyColors.c_f93, fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.circle,
                      size: 15,
                      color: MyColors.c_fdf,
                    ),
                    title: Text(
                      "Upload CV",
                      style: TextStyle(color: MyColors.c_b26, fontSize: 21),
                    ),
                    subtitle: Text(
                      "Curriculum Vitae",
                      style: TextStyle(color: MyColors.c_f93, fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.circle,
                      size: 15,
                      color: MyColors.c_fdf,
                    ),
                    title: Text(
                      "Upload Introductory Video",
                      style: TextStyle(color: MyColors.c_b26, fontSize: 21),
                    ),
                    subtitle: Text(
                      "Introduction video 3 min maximum",
                      style: TextStyle(color: MyColors.c_f93, fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
            ListView.separated(
                primary: false,
                shrinkWrap: true,
                itemCount: 5,
                separatorBuilder: (_, index) {
                  return SizedBox(
                    height: 15,
                  );
                },
                itemBuilder: (xtc, index) {
                  return Container(
                    color: Colors.white,
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Image.asset(
                            "images/approve.png",
                            width: 40,
                            height: 40,
                          ),
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Smart Trade",
                              style: TextStyle(
                                  color: MyColors.c_b26, fontSize: 18),
                            ),
                            Text(
                              "Senior Full Stack Developer",
                              style: TextStyle(
                                  color: MyColors.c_5e3, fontSize: 18),
                            ),
                            Text(
                              "Full-time . EGP7,000 - EGP10,000 / month ",
                              style: TextStyle(
                                  color: MyColors.c_f93, fontSize: 12),
                            ),
                            Text(
                              "Alexandria, Egypt",
                              style: TextStyle(
                                  color: MyColors.c_f93, fontSize: 12),
                            ),
                            Text(
                              "1 hour ago",
                              style: TextStyle(
                                  color: MyColors.c_f93, fontSize: 12),
                            )
                          ],
                        ))
                      ],
                    ),
                  );
                })
          ],
        ),
      ),
    );
  }
}
