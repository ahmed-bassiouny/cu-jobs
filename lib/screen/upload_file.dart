import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class UploadFileScreen extends StatefulWidget {
  UploadFileScreen({Key key}) : super(key: key);

  @override
  _UploadFileScreenState createState() => _UploadFileScreenState();
}

class _UploadFileScreenState extends State<UploadFileScreen> {
  var type = UploadFile.CV;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: FlatButton(
                  onPressed: () {
                    setState(() {
                      type = UploadFileHelper.getNext(type);
                    });
                  },
                  child: Text(
                    AppLocalizations.of(context)
                        .translate("skip")
                        .toUpperCase(),
                    style: TextStyle(color: MyColors.c_5e3, fontSize: 16),
                    textAlign: TextAlign.right,
                  )),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate(UploadFileHelper.getTitle(type)),
              style: TextStyle(
                  color: MyColors.c_a43,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),

            SizedBox(
              height: 10,
            ),
            Text(
              AppLocalizations.of(context)
                  .translate(UploadFileHelper.getInfo(type)),
              style: TextStyle(color: MyColors.c_78c, fontSize: 15),
              textAlign: TextAlign.center,
            ),

            SizedBox(
              height: 50,
            ),
            Center(
                child: Stack(
                  alignment: Alignment.center,
              children: [
                Image.asset("images/upload.png"),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0,40,0,0),
                  child: Text(
              AppLocalizations.of(context)
                    .translate("click_here"),
              style: TextStyle(color: MyColors.c_78c, fontSize: 15),
              textAlign: TextAlign.center,
            ),
                )
              ],
            )),

            SizedBox(
              height: 50,
            ),
            ButtonTheme(
              minWidth: 300.0,
              height: 50,
              child: RaisedButton(
                color: MyColors.c_fec,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  Navigator.of(context).pushNamed("/complete_profile");
                },
                child: Text(
                  AppLocalizations.of(context).translate("upload"),
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            //Center(child: CircularProgressIndicator()),
          ],
        ),
      ),
    );
  }
}

enum UploadFile { CV, VIDEO, IMAGE }

class UploadFileHelper {
  static String getTitle(UploadFile type) {
    switch (type) {
      case UploadFile.CV:
        return "upload_cv";
      case UploadFile.VIDEO:
        return "upload_video";
      case UploadFile.IMAGE:
        return "upload_image";
    }
    return "";
  }

  static String getInfo(UploadFile type) {
    switch (type) {
      case UploadFile.CV:
        return "upload_cv_info";
      case UploadFile.VIDEO:
        return "upload_video_info";
      case UploadFile.IMAGE:
        return "upload_image_info";
    }
    return "";
  }

  static UploadFile getNext(UploadFile type) {
    switch (type) {
      case UploadFile.CV:
        return UploadFile.VIDEO;
      case UploadFile.VIDEO:
        return UploadFile.IMAGE;
      case UploadFile.IMAGE:
        return UploadFile.CV;
    }
  }
}
