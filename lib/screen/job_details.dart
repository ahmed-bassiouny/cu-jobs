import 'package:cu_jobs/utils/MyColors.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/material.dart';

class JobDetailsScreen extends StatelessWidget {
  title(String txt) => Text(
        txt,
        style: TextStyle(
            color: MyColors.c_b26,
            fontWeight: FontWeight.bold,
            fontFamily: "english-light",
            fontSize: 18),
      );
  titleNotBold(String txt) => Text(
        txt,
        style: TextStyle(
            color: MyColors.c_b26, fontFamily: "english-light", fontSize: 18),
      );

  subTitle(String txt) => Text(
        txt,
        style: TextStyle(
            color: MyColors.c_b26.withOpacity(.7),
            fontFamily: "english-light",
            fontSize: 16),
      );

  subTitle_5(String txt, [double size = 16]) => Text(
        txt,
        style: TextStyle(
            color: MyColors.c_b26.withOpacity(.5),
            fontFamily: "english-light",
            fontSize: size),
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Job Details"),
        backgroundColor: MyColors.c_5e3,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: Image.asset(
                "images/approve.png",
                width: 60,
                height: 60,
              ),
              title: title("SMART TRADE"),
              subtitle: subTitle_5("Alexandria"),
            ),
            SizedBox(
              height: 40,
            ),
            title("Job Description"),
            subTitle(
                "We are looking for a Senior Software Engineer to design and implement new systems and features, as well as modify and maintain existing systems for adaptation to business and/or technology changes. The Senior Software Engineer engages directly with IT management, development teams, technical delivery teams, and vendors to ensure the successful design, development, and delivery of technology-based solutions."),
            SizedBox(
              height: 60,
            ),
            title("Requirements"),
            subTitle(
                "1- Bachelor's Degree in Computer Science or related field.\n2- Minimum 4 years of programming experience.\n 3- Broad experience designing, programming, and implementing large information systems.\n 4- Ability to provide in-depth evaluation and analysis of unique complex technological issues.\n 5- Excellent analytical and problem-solving skills.\n 6- Excellent organization and time management skills."),
            SizedBox(
              height: 80,
            ),
            title("Open For"),
            SizedBox(
              height: 10,
            ),
            Card(
              color: Colors.white,
              elevation: 1,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: Image.asset(
                      "images/imageicon1.png",
                      width: 60,
                      height: 60,
                    ),
                    title: subTitle_5("People who are looking for", 14),
                    subtitle: titleNotBold("Software Engineering"),
                  ),
                  Divider(),
                  ListTile(
                    leading: Image.asset(
                      "images/imageicon2.png",
                      width: 60,
                      height: 60,
                    ),
                    title: subTitle_5("Interested to work in", 14),
                    subtitle: titleNotBold("Alexandria, Egypt"),
                  ),
                  Divider(),
                  ListTile(
                    leading: Image.asset(
                      "images/imageicon3.png",
                      width: 60,
                      height: 60,
                    ),
                    title: subTitle_5("We are looking to fill a", 14),
                    subtitle: titleNotBold("Mid-level or Senior position"),
                  ),
                  Divider(),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              height: 150,
              child: ListView(scrollDirection: Axis.horizontal, children: [
                Container(
                  width: 250,
                  decoration: BoxDecoration(
                      color: MyColors.c_5e3,
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(10.0),
                      )),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "There are",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16),
                        ),
                        Text(
                          "11 - 50",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30),
                        ),
                        Text(
                          "Employees in the company",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                Container(
                  width: 250,
                  decoration: BoxDecoration(
                      color: MyColors.c_5e3,
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(10.0),
                      )),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16),
                        ),
                        Text(
                          "2015",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30),
                        ),
                        Text(
                          "Company was founded",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
            SizedBox(
              height: 30,
            ),
            title("About Company"),
            subTitle(
              ("Smart Trade was founded in September 2005 by Ali. Mohamed and Ahmed Esam while they were Ph.D. students at Stanford University in California. Together they own about 14 percent of its shares and control 56 percent of the stockholder voting power through supervoting stock."),
            ),
            SizedBox(
              height: 30,
            ),
            Center(
              child: ButtonTheme(
                minWidth: 300.0,
                height: 50,
                child: RaisedButton(
                  color: MyColors.c_2c2,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onPressed: () {
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("applied_before"),
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            )
          ],
        ),
      ),
    );
  }
}
