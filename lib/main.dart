import 'package:cu_jobs/screen/account_blocked.dart';
import 'package:cu_jobs/screen/account_created.dart';
import 'package:cu_jobs/screen/complete_profile.dart';
import 'package:cu_jobs/screen/easy_apply.dart';
import 'package:cu_jobs/screen/home.dart';
import 'package:cu_jobs/screen/job_details.dart';
import 'package:cu_jobs/screen/login.dart';
import 'package:cu_jobs/screen/upload_file.dart';
import 'package:cu_jobs/screen/verify_account.dart';
import 'package:cu_jobs/utils/app_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily:true ? "english" : "arabic",
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
       supportedLocales: [Locale('en', 'EN')],
              locale:Locale("en", "EN"),
              localizationsDelegates: [
                //class which loads the translation from json files
                AppLocalizations.delegate,
                // built in localization for material widget
                GlobalMaterialLocalizations.delegate,
                // built in localization for text directions ltr/rtl
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
      home: JobDetailsScreen(),
      debugShowCheckedModeBanner: false,
      routes: {
                '/verify_account': (context) => VerifyAccountScreen(),
                '/complete_profile': (context) => CompleteProfileScreen(),
                '/account_created': (context) => AccountCreatedScreen(),
                '/blocked': (context) => AccountBlockedScreen(),
              }
    );
  }
}

